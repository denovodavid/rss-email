use chrono::{Duration, Utc};
use dotenvy::dotenv;
use futures::future;
use lambda_runtime::{service_fn, Error, LambdaEvent};
use lettre::{transport::smtp, AsyncTransport};
use lol_html::{element, HtmlRewriter};
use serde_json::{json, Value};
use std::env;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let handler = service_fn(do_stuff);
    lambda_runtime::run(handler).await?;
    Ok(())
}

async fn do_stuff(_event: LambdaEvent<Value>) -> Result<Value, Error> {
    dotenv().ok();

    //==== mail setup ====//
    let from_email = &env::var("FROM_EMAIL")?;
    let to_email = &env::var("to_EMAIL")?;
    let smtp_username = env::var("SMTP_USERNAME")?;
    let smtp_password = env::var("SMTP_PASSWORD")?;
    let smtp_host = env::var("SMTP_HOST")?;
    let smtp_port: u16 = env::var("SMTP_PORT")?.parse()?;
    let creds = smtp::authentication::Credentials::new(smtp_username, smtp_password);
    let mailer: &lettre::AsyncSmtpTransport<lettre::Tokio1Executor> =
        &lettre::AsyncSmtpTransport::<lettre::Tokio1Executor>::relay(&smtp_host)?
            .port(smtp_port)
            .credentials(creds)
            .build();
    if mailer.test_connection().await? == false {
        return Err("Cannot connect to mail server".into());
    }

    //==== check feeds ====//
    let time_24_hours_ago = Utc::now() - Duration::days(7 * 8);
    let feed_urls = env::var("FEED_URLS")?;
    let feed_futures = feed_urls.split(' ').map(|feed_url| async move {
        let content = reqwest::get(feed_url).await?.bytes().await?;
        let feed = &feed_rs::parser::parse(&content[..])?;
        let mut entry_futures = vec![];
        for entry in &feed.entries {
            let err = || Error::from("oh well");
            let pub_date = entry.published.ok_or(err())?;
            if pub_date < time_24_hours_ago {
                break;
            } else {
                entry_futures.push(async move {
                    //==== transform urls ====//
                    let entry_link = url::Url::parse(&entry.links[0].href)?;
                    let mut entry_content = vec![];
                    let mut rewriter = HtmlRewriter::new(
                        lol_html::Settings {
                            element_content_handlers: vec![
                                element!("a[href]", |el| {
                                    let href = entry_link.join(
                                        &el.get_attribute("href").expect("href was required"),
                                    )?;
                                    el.set_attribute("href", href.as_str())?;
                                    Ok(())
                                }),
                                element!("img[src]", |el| {
                                    let src = entry_link.join(
                                        &el.get_attribute("src").expect("src was required"),
                                    )?;
                                    el.set_attribute("src", src.as_str())?;
                                    Ok(())
                                }),
                            ],
                            ..lol_html::Settings::default()
                        },
                        |c: &[u8]| entry_content.extend_from_slice(c),
                    );
                    let entry_body = entry
                        .content
                        .clone()
                        .and_then(|c| c.body)
                        .or(entry.summary.clone().map(|s| s.content))
                        .ok_or(err())?;
                    rewriter.write(entry_body.as_bytes())?;

                    //==== build email ====//
                    let feed_title = &feed.title.clone().ok_or(err())?.content;
                    let entry_title = &entry.title.clone().ok_or(err())?.content;
                    let entry_link = entry_link.as_str();
                    let mailbox = lettre::message::Mailbox::new(
                        Some(format!("RSS | {feed_title}")),
                        from_email.parse()?,
                    );
                    let body = lettre::message::Body::new(format!(
                        r#"<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{entry_title}</title>
</head>
<body>
	<h1><a href="{entry_link}">{entry_title}</a></h1>
	<p><b>{feed_title}</b> | {pub_date}</p>
	{entry_content}
</body>
</html>"#,
                        pub_date = pub_date.format("%e %b %Y"),
                        entry_content = String::from_utf8(entry_content)?
                    ));
                    let email = lettre::Message::builder()
                        .from(mailbox)
                        .to(to_email.parse()?)
                        .subject(entry_title)
                        .header(lettre::message::header::ContentType::TEXT_HTML)
                        .body(body)?;

                    //==== send email ====//
                    mailer.send(email).await?;
                    println!("Email Sent: {feed_title} | {entry_title}");
                    Ok::<(), Error>(())
                });
            }
        }
        future::join_all(entry_futures).await;
        Ok::<(), Error>(())
    });
    future::join_all(feed_futures).await;

    Ok(json!({}))
}
