# rss-email

A little function to send me emails about new RSS posts.

## implementations

go, rust

### go

- easy to do async with goroutines
- gofeed is resilient to rss/atom weirdness
- error handling is simple but easy to ignore
- easy to develop and deploy

### rust

- slow dx
- must handle or return every error
- incompatible error types are annoying
- feed_rs doesn't seem to like some feeds (needs investigating)
- i suppose it's fast
