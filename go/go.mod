module gitlab.com/denovodavid/rss-email

go 1.19

require (
	github.com/aws/aws-lambda-go v1.37.0
	github.com/joho/godotenv v1.4.0
	github.com/mmcdole/gofeed v1.2.0
	github.com/xhit/go-simple-mail/v2 v2.13.0
	golang.org/x/net v0.5.0
)

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/go-test/deep v1.1.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mmcdole/goxpp v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/toorop/go-dkim v0.0.0-20201103131630-e1cd1a0a5208 // indirect
	golang.org/x/text v0.6.0 // indirect
)
