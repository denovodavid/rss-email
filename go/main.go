package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/joho/godotenv"
	"github.com/mmcdole/gofeed"
	mail "github.com/xhit/go-simple-mail/v2"
	"golang.org/x/net/html"
)

func main() {
	lambda.Start(doStuff)
}

func doStuff() {
	godotenv.Load()

	logFatal := log.Fatalln
	logError := log.Println
	logInfo := log.New(os.Stdout, "info: ", log.LstdFlags).Println

	//==== smtp setup ====//
	server := mail.NewSMTPClient()
	server.Host = os.Getenv("SMTP_HOST")
	port, err := strconv.ParseInt(os.Getenv("SMTP_PORT"), 10, 32)
	if err != nil {
		logFatal(err)
	}
	server.Port = int(port)
	server.Username = os.Getenv("SMTP_USERNAME")
	server.Password = os.Getenv("SMTP_PASSWORD")
	server.Encryption = mail.EncryptionSSLTLS
	{
		// NOTE(david): test server connection
		smtpClient, err := server.Connect()
		if err != nil {
			logFatal(err)
		}
		smtpClient.Close()
	}

	//==== check feeds ====//
	feedURLs := strings.Split(os.Getenv("FEED_URLS"), " ")
	var wg sync.WaitGroup
	wg.Add(len(feedURLs))
	for _, feedUrl := range feedURLs {
		go func(feedUrl string) {
			defer wg.Done()
			feedParser := gofeed.NewParser()
			feed, err := feedParser.ParseURL(feedUrl)
			if err != nil {
				logError(err)
				return
			}
			// time24HoursAgo := time.Now().Add(-time.Hour * 24 * 7 * 8)
			time24HoursAgo := time.Now().Add(-time.Hour * 24)
			for _, item := range feed.Items {
				if item.PublishedParsed.After(time24HoursAgo) {
					wg.Add(1)
					go func(item gofeed.Item) {
						defer wg.Done()

						//==== transform urls ====//
						itemLink, err := url.Parse(item.Link)
						if err != nil {
							logFatal(err)
						}
						itemContent := item.Content
						if itemContent == "" {
							itemContent = item.Description
						}
						itemBody, err := html.Parse(strings.NewReader(itemContent))
						if err != nil {
							logError(err)
							return
						}
						var transformUrls func(*html.Node)
						transformUrls = func(n *html.Node) {
							if n.Type == html.ElementNode && (n.Data == "a" || n.Data == "img") {
								for idx, a := range n.Attr {
									if a.Key == "href" || a.Key == "src" {
										u, err := url.Parse(a.Val)
										if err != nil {
											logError(err)
											break
										}
										a.Val = itemLink.ResolveReference(u).String()
										n.Attr[idx] = a
										break
									}
								}
							}
							for c := n.FirstChild; c != nil; c = c.NextSibling {
								transformUrls(c)
							}
						}
						transformUrls(itemBody)
						var buf bytes.Buffer
						w := io.Writer(&buf)
						err = html.Render(w, itemBody)
						if err != nil {
							logFatal(err)
						}
						content := buf.String()

						//==== build email ====//
						// TODO: try source from item.Image first
						image := ""
						if item.Custom["image"] != "" {
							image = fmt.Sprintf(
								emailImageTemplate,
								item.Custom["image"],
								item.Title+" main image",
							)
						}
						body := fmt.Sprintf(
							emailTemplate,
							item.Title,
							image,
							item.Link,
							item.Title,
							feed.Title,
							item.PublishedParsed.Format("2 Jan 2006"),
							content,
						)
						email := mail.NewMSG()
						email.SetFrom("RSS | "+feed.Title+" <"+os.Getenv("FROM_EMAIL")+">").
							AddTo(os.Getenv("TO_EMAIL")).
							SetSubject(item.Title).
							SetBody(mail.TextHTML, body)
						if email.Error != nil {
							logError(email.Error)
						}

						//==== send email ====//
						smtpClient, err := server.Connect()
						if err != nil {
							logError(err)
							return
						}
						defer smtpClient.Quit()
						err = email.Send(smtpClient)
						if err != nil {
							logError(err)
						} else {
							logInfo("Email Sent: " + feed.Title + " | " + item.Title)
						}
					}(*item)
				} else {
					break
				}
			}
		}(feedUrl)
	}
	wg.Wait()
}

const emailTemplate = `<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>%s</title>
	</head>
	<body>
		%s
		<h1><a href="%s">%s</a></h1>
		<p><b>%s</b> | %s</p>
		%s
	</body>
</html>`

const emailImageTemplate = `<img src="%s" alt="%s" style="width: 100%%; height: auto; border-radius: 8px; margin-top: 16px;">`
