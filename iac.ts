#!/usr/bin/env node
import * as cdk from "aws-cdk-lib";
import * as path from "path";

import "dotenv/config";

export class RssEmailStack extends cdk.Stack {
	constructor(app: cdk.App, id: string, props: cdk.StackProps) {
		super(app, id, props);
		const handler = new cdk.aws_lambda.Function(this, "handler", {
			architecture: cdk.aws_lambda.Architecture.ARM_64,
			memorySize: 128,
			runtime: cdk.aws_lambda.Runtime.PROVIDED_AL2,
			code: new cdk.aws_lambda.AssetCode(path.join(__dirname, "dist")),
			handler: "main",
			timeout: cdk.Duration.seconds(30),
			retryAttempts: 0,
			environment: {
				SMTP_HOST: process.env.SMTP_HOST!,
				SMTP_PORT: process.env.SMTP_PORT!,
				SMTP_USERNAME: process.env.SMTP_USERNAME!,
				SMTP_PASSWORD: process.env.SMTP_PASSWORD!,
				FROM_EMAIL: process.env.FROM_EMAIL!,
				TO_EMAIL: process.env.TO_EMAIL!,
				FEED_URLS: process.env.FEED_URLS!,
			},
		});
		new cdk.aws_events.Rule(this, "schedule", {
			// run 7:00 PM UTC every day
			schedule: cdk.aws_events.Schedule.expression("cron(0 19 * * ? *)"),
			targets: [new cdk.aws_events_targets.LambdaFunction(handler)],
		});
	}
}

const app = new cdk.App();
new RssEmailStack(app, "rss-email", {
	env: { account: "853874849328", region: "ap-southeast-2" },
});
